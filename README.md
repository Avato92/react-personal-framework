# React application base project

## Summary

This project is a basic framework for starting any React application, with eslint, prettier, and git-hooks.

It is designed to have the entire project configured directly with a good base of good practices.

**Note: You need to have the eslint and prettier VSC extensions installed**

[eslint-extension](https://marketplace.visualstudio.com/items?itemName=dbaeumer.vscode-eslint)

[prettier-extension](https://marketplace.visualstudio.com/items?itemName=esbenp.prettier-vscode)

Is possible you need to configure something else.

I attached my configuration from the VSC `settings.json`

```json
{
  "eslint.run": "onSave",
  "files.autoSave": "onFocusChange",
  "editor.codeActionsOnSave": {
    "source.fixAll.eslint": true
  },
  "eslint.alwaysShowStatus": true,
  "editor.defaultFormatter": null,
  "[javascript]": {
    "editor.defaultFormatter": "esbenp.prettier-vscode",
    "editor.formatOnSave": true
  }
}
```

**How to configure settings.json**

To open the settings.json file, you must press `Ctrl + ,` and then press the button on the top right that says Open Settings (JSON)

With this configuration eslint runs when you save the file.

You activate auto-save mode when you switch windows.

You deactivate auto format on save, but you activate eslint and runs `eslint --fix`

**Important: To resolve the conflicts between eslint and prettier, we use the library eslint-config-prettier**

### linter

This project have airbnb rules, if you want to use another rules, you only need to use:

`npx linter --init`

And select your options

### git-hooks

To prevent changes from being uploaded to git without the proper formatting, I have added the `lint-staged` library, which adds `husky`.

If you look at the directory, you will find the `.husky` folder and inside you will see that what it does is execute a bash before making a commit.

If the result of this bash is an error, the commit will not be executed.

## Tools

### [eslint](https://eslint.org/)

### [prettier](https://prettier.io/)

### [husky](https://typicode.github.io/husky/#/)

### [react](https://es.reactjs.org/)

### [lint-staged](https://github.com/okonet/lint-staged)

## Commands

Some typicals commands

### `npm start`

Start the application on development mode

### `npm run build`

Create a build of the project

### `npm run lint`

Linter checks the code and return errors and warnings

### `npm run lint-fix`

Linter checks the code and corrects it

### `npm run prettier`

Prettier checks and corrects code

### `npm run prepare`

This command installs husky and creates the folder and shell scripts

### `npm run coverage`

To see the coverage of all the tests

## Improvements

At the moment, I don't have more ideas to add, if you have more improvements, please contact me!

## Contact

If you liked this framework and want to contact me to correct errors or add new features.

Send me an email to the address: avato92@gmail.com
