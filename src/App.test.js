import { fireEvent, render, screen } from "@testing-library/react";
import "@testing-library/jest-dom/extend-expect";
import App from "./App";

test("renders learn react link", () => {
  render(<App />);
  const linkElement = screen.getByText(/learn react/i);
  expect(linkElement).toBeInTheDocument();
});

/**
 * How to check methods inside the component
 */
test("checks input label", () => {
  render(<App />);
  const addBtn = screen.getByText("+");
  fireEvent.click(addBtn);

  let span = screen.getByText(1);

  expect(span).toBeInTheDocument();

  const substractBtn = screen.getByText("-");
  fireEvent.click(substractBtn);

  span = screen.getByText(0);

  expect(span).toBeInTheDocument();
});
