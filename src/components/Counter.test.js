import { fireEvent, render, screen } from "@testing-library/react";
import "@testing-library/jest-dom/extend-expect";
import Counter from "./Counter";

/**
 * How to debug?
 * component.debug()
 *
 * This method prints all the HTML of the component
 *
 * If you prefer to debug manually, here is another example of how to do it
 *
 * import { prettyDOM } from "@testing-library/dom";
 *
 * And then, inside the test
 *
 *    const component = render(
 *      <Counter count={counter.count} handleClick={counter.handleClick} />
 *    );
 *
 *    const btn = component.container.querySelector("button");
 *    console.log(prettyDOM(btn));
 */

test("renders counter", () => {
  const counter = {
    count: 3,
    handleClick: () => {},
  };
  render(<Counter count={counter.count} handleClick={counter.handleClick} />);

  screen.getByText("+");
  screen.getByText(counter.count);
  screen.getByText("-");
});

test("renders counter another way to test", () => {
  const counter = {
    count: 3,
    handleClick: () => {},
  };
  const component = render(
    <Counter count={counter.count} handleClick={counter.handleClick} />
  );
  expect(component.container).toHaveTextContent(counter.count);

  const el = component.getByText("+");

  expect(el).toBeDefined();
});

test("clicking the sum button calls event handler once", () => {
  const count = 3;
  /**
   * Mock a function
   */
  const mockHandler = jest.fn();
  render(<Counter count={count} handleClick={mockHandler} />);

  const btn = screen.getByText("+");
  fireEvent.click(btn);

  /**
   * Two ways to check how many times is called a function
   */

  // expect(mockHandler.mock.calls).toHaveLength(1);
  expect(mockHandler).toHaveBeenCalledTimes(1);

  const substractBtn = screen.getByText("-");
  fireEvent.click(substractBtn);

  expect(mockHandler).toHaveBeenCalledTimes(2);
});

/**
 * Another way, in this example we don't need to render the same component all the time
 */

describe("<Counter />", () => {
  const count = 3;
  const mockHandler = jest.fn();

  beforeEach(() => {
    render(<Counter count={count} handleClick={mockHandler} />);
  });

  test("renders counter", () => {
    screen.getByText(count);
  });

  /**
   * How to check css properties
   */
  test("check style properties", () => {
    const btn = screen.getByText("+");
    expect(btn).toHaveStyle("display: inline-block;");
  });
});
