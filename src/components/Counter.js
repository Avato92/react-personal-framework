import PropTypes from "prop-types";
import "./Counter.css";

const Counter = ({ count, handleClick }) => (
  <div className="container__btns">
    <button
      type="button"
      onClick={handleClick}
      value="subtraction"
      className="btn-primary"
    >
      -
    </button>
    <span className="text-primary">{count}</span>
    <button
      type="button"
      onClick={handleClick}
      value="sum"
      className="btn-primary"
    >
      +
    </button>
  </div>
);

Counter.propTypes = {
  count: PropTypes.number.isRequired,
  handleClick: PropTypes.func.isRequired,
};

export default Counter;
