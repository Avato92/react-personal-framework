import { useState } from "react";
import logo from "./logo.svg";
import "./App.css";
import Counter from "./components/Counter";

function App() {
  const [count, setCount] = useState(0);

  const handleClick = (e) => {
    if (e.target.value === "subtraction") {
      setCount(count - 1);
    } else {
      setCount(count + 1);
    }
  };

  return (
    <div className="App">
      <header className="App-header">
        <img src={logo} className="App-logo" alt="logo" />
        <a
          className="App-link"
          href="https://reactjs.org"
          target="_blank"
          rel="noopener noreferrer"
        >
          Learn React
        </a>
        <Counter count={count} handleClick={handleClick} />
      </header>
    </div>
  );
}

export default App;
